def turn_right ():
    turn_left() 
    turn_left()
    turn_left()

def jump ():
    turn_left()
    while wall_on_right():
        move()
    turn_right()

def descend ():
    move ()
    turn_right()
    move()
    while wall_on_right() and not wall_in_front():
        move()
    turn_left()
    
while not at_goal():
    if not wall_in_front() and not wall_on_right():
        descend ()
    elif wall_in_front() and wall_on_right():
        jump()
    else:
        move()
